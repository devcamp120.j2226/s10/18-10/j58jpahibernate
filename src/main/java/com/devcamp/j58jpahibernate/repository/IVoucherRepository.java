package com.devcamp.j58jpahibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j58jpahibernate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
