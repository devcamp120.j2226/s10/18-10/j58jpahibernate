package com.devcamp.j58jpahibernate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j58jpahibernate.model.CVoucher;
import com.devcamp.j58jpahibernate.repository.IVoucherRepository;

@RestController
@CrossOrigin
public class CVoucherController {   
    @Autowired
    IVoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ArrayList<CVoucher> getListVouchers() {
        ArrayList<CVoucher> lstVoucher = new ArrayList<>();
        voucherRepository.findAll().forEach(lstVoucher::add);

        return lstVoucher;
    }

    @GetMapping("/vouchers2")
    public ResponseEntity<List<CVoucher>> getListVouchers2() {
        try {
            ArrayList<CVoucher> lstVoucher = new ArrayList<>();
            voucherRepository.findAll().forEach(lstVoucher::add);
            if (lstVoucher.size() == 0) {
                return new ResponseEntity<>(lstVoucher, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(lstVoucher, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
