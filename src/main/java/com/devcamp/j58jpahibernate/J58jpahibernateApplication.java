package com.devcamp.j58jpahibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J58jpahibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(J58jpahibernateApplication.class, args);
	}

}
